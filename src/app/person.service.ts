import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import {Person} from './person'
import { Observable } from 'rxjs/Rx';
import { DataTableParams } from './data-table';
import {DataResponse} from './data.response'

function	paramsToQueryString(params: DataTableParams) {
    let result = [];

    if (params.offset != null) {
        result.push(['_start', params.offset]);
    }
    if (params.limit != null) {
        result.push(['_limit', params.limit]);
    }
    if (params.sortBy != null) {
        result.push(['_sort', params.sortBy]);
    }
    if (params.sortAsc != null) {
        result.push(['_order', params.sortAsc ? 'ASC' : 'DESC']);
    }

    return result.map(param => param.join('=')).join('&');
}

@Injectable()
export class PersonService {

	apiRoot: string = 'http://127.0.0.1:8000/mlrecruiterpal';

  constructor(private http: Http) { }

  getCandidates(params:DataTableParams){
  	
  	let apiUrl = this.apiRoot +'/candidates/?'+ paramsToQueryString(params);

  	return this.http.get(apiUrl).map((res: Response) => res.json())
  }

  getEmployees(params:DataTableParams){
    
    let apiUrl = this.apiRoot +'/employees/?'+ paramsToQueryString(params);
    return this.http.get(apiUrl).map((res: Response) => res.json())
  }

  evaluate(personId){
    let apiUrl = this.apiRoot +'/evaluate/?_id='+ personId;
    return this.http.get(apiUrl).map((res: Response) => res.json())
  }

  evaluateAll(data){

    let ids = [];
    
    data.forEach(function(item) {
      ids.push({id:item.id});
    });

    let apiUrl = this.apiRoot +'/evaluateAll/';
  
    return this.http.post(apiUrl, data=JSON.stringify(ids)).map((res: Response) => res.json())
  
  }

  evaluateOne(candidate){
    let apiUrl = this.apiRoot +'/evaluateOne/';
    let data=JSON.stringify(candidate);
    
    return this.http.post(apiUrl, data).map((res: Response) => res.json())

  }


}
