/**
* Class Person
*/
export class Person {
	constructor(
							public id: number, 
							public name: string, 
							public age_range: string, 
							public sex: string, 
							public nacionality: string,
							public birth_city: string,
							public max_education: string,
							public idioms: string,
							public experience: string,
							public job_average: string,
							public skill_leadership: string,
							public skill_social: string,
							public skill_creativity: string,
							public skill_responsability : string, 
							public score : string,
							public segment : string) {}
}