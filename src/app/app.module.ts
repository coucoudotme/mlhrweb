import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes }   from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpModule, Http, Response } from '@angular/http';
import 'rxjs/Rx';
import { PersonService} from './person.service'
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { DataTableModule } from './data-table';


import { AppComponent } from './app.component';
import { EvaluatedEmployeesComponent } from './evaluated-employees/evaluated-employees.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { CandidatePeopleComponent } from './candidate-people/candidate-people.component';
import { CandidateFormComponent } from './candidate-form/candidate-form.component';

const appRoutes: Routes = [
  { path: 'candidate-form',   component: CandidateFormComponent },
  { path: 'evaluated-employees',   component: EvaluatedEmployeesComponent },
  { path: 'candidate-people',   component: CandidatePeopleComponent },
  { path: '', redirectTo: '/candidate-people', pathMatch: 'full' },
  { path: '**',                 component: PageNotFoundComponent }

];


@NgModule({
  declarations: [
    AppComponent,
    EvaluatedEmployeesComponent,
    PageNotFoundComponent,
    CandidatePeopleComponent,
    CandidateFormComponent
  ],
  imports: [
    BrowserModule,
    NgbModule.forRoot(), 
  	FormsModule,
  	HttpModule,
    RouterModule.forRoot(
      appRoutes,
      {enableTracing:true}
    ),
    DataTableModule
  ],
  providers: [PersonService],
  bootstrap: [AppComponent]
})
export class AppModule { }
