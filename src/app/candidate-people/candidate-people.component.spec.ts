import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CandidatePeopleComponent } from './candidate-people.component';

describe('CandidatePeopleComponent', () => {
  let component: CandidatePeopleComponent;
  let fixture: ComponentFixture<CandidatePeopleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CandidatePeopleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CandidatePeopleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
