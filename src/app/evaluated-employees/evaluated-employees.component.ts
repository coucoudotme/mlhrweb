import { Component, OnInit } from '@angular/core';
import { PersonService } from '../person.service';
import { Person} from '../person';
import {DataResponse} from '../data.response'

import { Observable } from 'rxjs/Rx';

@Component({
  selector: 'app-evaluated-employees',
  templateUrl: './evaluated-employees.component.html',
  styleUrls: ['./evaluated-employees.component.css']
})
export class EvaluatedEmployeesComponent implements OnInit {

  items = [];
  itemCount = 0;
  
  constructor(private service: PersonService) {}

  reloadItems(params) {
  		
  	this.service.getEmployees(params).subscribe( (response: DataResponse) => {
  		this.itemCount = response.count;
  		this.items = response.result.map(person=> {
  			return new Person(person.id, 
													person.name, 
													person.age_range, 
													person.sex, 
													person.nacionality,
													person.birth_city,
													person.max_education,
													person.idioms,
													person.experience,
													person.job_average,
													person.skill_leadership,
													person.skill_social,
													person.skill_creativity,
													person.skill_responsability, 
													person.score,
													person.segment
  			)
  		});
  	})
	}	


  ngOnInit() {
  }

    personClicked(person) {
      alert(person);
  }

  rowClick(rowEvent) {
      console.log('Clicked: ' + rowEvent.row.item.name);
      this.service.evaluate(rowEvent.row.item.id).subscribe( (response: object) => {
      	console.log(response);
      })
  }

  rowDoubleClick(rowEvent) {
      alert('Nombre: ' + rowEvent.row.item.name +'\n' + 'Calificación: '+ rowEvent.row.item.score);
  }

  rowTooltip(item) { 
  	return item.score; 
  }

}
