import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EvaluatedEmployeesComponent } from './evaluated-employees.component';

describe('EvaluatedEmployeesComponent', () => {
  let component: EvaluatedEmployeesComponent;
  let fixture: ComponentFixture<EvaluatedEmployeesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EvaluatedEmployeesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EvaluatedEmployeesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
