import { Component, OnInit } from '@angular/core';
import { PersonService } from '../person.service';
import { Person} from '../person';
import {DataResponse} from '../data.response'

import { Observable } from 'rxjs/Rx';


@Component({
  selector: 'app-candidate-form',
  templateUrl: './candidate-form.component.html',
  styleUrls: ['./candidate-form.component.css']
})
export class CandidateFormComponent implements OnInit {

  candidate = new Person(0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
  staticAlertClosed = true;

  constructor(private service: PersonService) { }

  ngOnInit() {
  }

  onSubmit() {
    console.log(JSON.stringify(this.candidate));
 	  this.service.evaluateOne(this.candidate).subscribe( (response) => {
    	this.staticAlertClosed = !this.staticAlertClosed;
    	console.log(response);
    });
  }

}
