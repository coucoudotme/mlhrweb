import {Person} from './person'

export interface DataResponse{
	result: Person [];
	count:number;
}
